# Player character (PC) parser

The ``pc`` command parses RPG character sheets written in the INI format.
It works with any system as long as that system's character sheet data can be expressed as basic INI.

## Requirements

This command is written in Perl.
You must install Perl to run this script.

On Linux, BSD, and Mac, you already have Perl installed.

On Windows, you can install Perl from [perl.org](https://www.perl.org/get.html#win32).

## Install

To install, you can copy `src/pc.pl` to some location in your `$PATH`, and make it executable.
For example:

```bash
$ cp src/pc.pl /usr/local/bin/pc
$ chmod +x /usr/local/bin/pc
```

You can alternately use cmake:

```bash
$ mkdir build
$ cd build
$ cmake ..
$ cmake --install .
```

## The INI file format

An INI file consists of a category in `[brackets]` and a group of key and value pairs (such as `foo=bar`). 
For example:

```
[Character]
Name=Skullerix
Level=2
Class=Wizard

[Health]
HP=6
max.HP=8
AC=12
equip.AC=leather armor
```

## Usage

The syntax for the `pc` command is followed by a **character** and usually the **stat**, or category of stats, you want to see.
For example:

```bash
$ pc skullerix HP
HP=6
```

Some game systems use the same keyword within several different categories.
For instance, each skill in a skill-absed system may use the keyword **level**, meaning you get ambiguous results with a simple search:

```bash
$ pc bitscum Level
Level=1
Level=3
```

To narrow your search, provide a specific category first:

```bash
$ pc bitscum Computers Level
Level=1
$ pc bitscum Chainsaw Level
Level=3
```

### Get a group of stats 

If an attribute matches a category, you see the whole category instead of just one stat:

```bash
$ pc skullerix Health
[Health]
AC=10
max.HP=8
HP=6
```

### Quick lookup

You usually don't have to type the full attribute.
As long as what you type is sufficiently unique, `pc` can usually figure out what you want:

```bash
$ pc skullerix Wea
[Weapons]
Dagger=1d4 piercing finesse light thrown (20/60)
Sword.great=1d8 slashing heavy two-handed
```

### Get the whole character sheet

Providing no stat prints the entire character sheet.

```bash
$ pc skullerix
[Character]
Name=Skullerix
Ancestry=Tiefling
Background=Hermit
Class=Wizard
Level=1

[Health]
AC=10
max.HP=8
HP=6
[...]
```

## Comments

In the INI spec, a comment is a line prefaced with a semicolon (`;`) symbol.
A comment doesn't show up when you look at a file with `pc`, but is in the file for your convenience when editing your character sheet.

```
[Health]
AC=10
max.HP=8
; remember to add CON mod when leveling
HP=6
```

## Updating a value

Battles in an RPG often requires you to track your character's HP (Health or Hit Points) based on whether your PC takes damage.
You can update values in your character sheet with the `--set` (`-s` for short) option.

```bash
$ pc skullerix HP
HP=8
$ pc skullerix HP --set 5
HP=5
HP updated to 5
$ pc skullerix HP
HP=5
```

The `pc` command is not intended to be a complete editor.
You *can* edit your entire sheet with `pc`, but it won't be very convenient.
Do not use an office suite or word processor, because they add a bunch of metadata that clutters the file up when viewed through `pc`).
Instead, use a proper [text editor](https://opensource.com/article/21/2/open-source-text-editors) such as [Atom](https://atom.io) when you level up.

### Multiple characters

If you play in several games, or build more than just one player character, or you're a DM with lots of NPCs to track, you can store several character sheets in `~/.local/share/pc` and query them without specifying the full path or extension.

For instance, if you have `jett.ini` and `bitscum.ini` saved in `~/.local/share/pc`, then you can query them by name:

```bash
$ pc jett Race
Ork
$ pc bitscum Class
Elf
```

To see all character sheets in your character sheet directory, use the `--list` (`-l` for short) option:

```bash
$ pc --list
abadon
bitscum
jett
skullerix
```

Character names are case-sensitive.
If you save a file as `skullerix.ini` then you must type `skullerix` to view the character sheet, not `SKULLERIX` or `SkulleRix`.

## Filenames and levels

Characters in class-based RPG games usually advance in level.
By default, `pc` looks for the highest level, as long as you name your character sheet files in the format `NAME_LEVEL.ini`.

For example, suppose you've been playing a character named `skullerix` for a few months.
Skullerix started at level 1 and has advanced to level 4.

```bash
$ pc --list
skullerix_1
skullerix_2
skullerix_3
skullerix_4
```

When you view skullerix's AC with `pc`, you always get the highest available level:

```bash
$ pc skullerix AC
AC=16
```

If for some reason you want to see a previous level, use the `--level` or `-v` option:

```bash
$ pc skullerix --level 1 AC
AC=10
```

## Alternate character sheet location

By default, your character sheets must be stored in the `~/.local/share/pc` directory.
You can override this with the ``--data`` or ``-d`` option.
For example, to look at character sheets in `~/rpg` instead:

```bash
$ pc --data ~/rpg abadon AC
AC=13
```

## Alternate file extension

Some operating systems and text editors editing files with unique file extensions difficult.
If your operating system is having a difficult time recognizing a file ending in `.ini` as a text file, that makes keeping character sheets in `.ini` inconvenient for you.

To override the file extension that `pc` expects, use the `--extension` (`-e` for short) option:

```bash
$ pc --extension txt skullerix AC
AC=16
```

This too can become inconvenient, so feel free to edit the `pc` script itself (in a text editor).
Look for this line:

```perl
my $ext = "ini";
```

And change it to:

```perl
my $ext = "txt";
```

## Bugs

File bug reports, feature requests, or brilliant ideas as Gitlab issues.
This is an open source project, so feel free to contribute code patches, too.

