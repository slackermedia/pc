#!/usr/bin/env perl

=begin license
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
=end license
=cut

use warnings;
use strict;
use utf8;
use IO::Handle;
use File::HomeDir;
use Getopt::Long;
use Data::Dumper;
use File::Path qw( make_path );

## main

my $dir = "pc";
my $data = File::HomeDir->my_data;
my $level;
my $help;
my $list;
my $dh;
my $num;
my @files;
my @results;
my $set;
my $cat;
my $ext = "ini";
my @type;
my $debug;

# make data dir if not exist
unless (-d "$data/$dir") {
    make_path("$data/$dir");
}

Getopt::Long::Configure(qw{no_ignore_case_always});

GetOptions ("v|level=i" => \$level,
            "data=s" => \$data,
            "extension=s" => \$ext,
            "l|list" => \$list,
            "c|cat" => \$cat,            
            "s|set=s" => \$set,
            "D|debug" => \$debug,
            "help" => \$help)
    or die printHelp();

parseOptions();

my $filename = shift or printHelp();

## parse filename
if ($level) {
    getLevel($filename);
} else {
    getCharacter($filename);
}

## get file
open my $fh, '<:encoding(UTF-8)', "$filename" or
    die "Could not open '$data/$dir/$filename'\n";

my $q = @ARGV;
if ($cat) {
    listCategories();
} elsif ($q < 1) {
    printAll();
} elsif ($q > 1) {
    getComplex();
} else {
    getSimple();
}

## if unique result exists, and set flag is true
if ($set && checkResults(@results)) {
    setStat(@results, $_, $set);
}

close($fh);
rename("$filename.tmp", $filename) if $set;

## subroutines

sub checkResults {
    my $n = @results;
    if ( $n <= 2 && $n > 0 ) {
        return(1);
    } else {
        die "Multiple matches for query.\n";
    }
}

sub dumpArgs {
    # this is not used
    # just for debugging
    print "$ARGV[0]\n";
    shift(@ARGV);
    print "$ARGV[0]\n";
}

sub setStat {
    print "setStat\n";
    open my $outfile, '>', "$filename.tmp" or die "Can't write new file: $!";

    #return to top of file
    seek($fh,0,0);
    my $c = 0;
    
    while ( <$fh> ) {
        $c++;
        s/(?<=$results[0]=)$results[1]/$set\n/ if $c == $num;
        print $outfile $_;
    }

    print "Updated $results[0] to $set\n";
    close $outfile;
}

sub getSimple {
    print "getSimple\n" if $debug;
    seek($fh,0,0);
    while(<$fh>) {
        if ($_ =~ /^\s*$/) {
            #whitespace
            next;
        }

        if ($_ =~ /^\s*\;/) {
            #comments
            next;
        }

        if ($_ =~ /^\[(.*)\]\s*$/ && $_ =~ $ARGV[0]) {
            #print whole section
            print "$_";
            $_ = <$fh>;

            while ($_ && $_ !~ /^\[(.*)\]\s*$/) {
                print "$_" if $_ !~ /^\s*\;/;
                $_ = <$fh>;
            }
        } elsif ($_ =~ /^$ARGV[0]/ && $_ !~ /^\[(.*)\]\s*$/) {
            ## print individual stat
            print $_;
            $num = $fh->input_line_number();
            push(@results, split /[=:]/, $_);
        } #fi
    }
}

sub getComplex {
    print "getComplex\n" if $debug;
    seek($fh,0,0);
    while(<$fh>){
        if ($_ =~ /^\s*$/) {
            #whitespace
            next;
        }

        if ($_ =~ /^\s*\;/) {
            #comments
            next;
        }
    
        if ($_ =~ /^\[(.*)\]\s*$/ && $_ =~ $ARGV[0]) {
            print "$_";
            shift(@ARGV);
            $_ = <$fh>; #advance cursor

            while (!$num && $_ !~ /^\[(.*)\]\s*$/) {
                if ($_ =~ /$ARGV[0]/ && $_ !~ /^\s*\;/) {
                    $num = $fh->input_line_number();
                    print "$_\n";
                    print "$num\n" if $debug;
                    push(@results, split /[=:]/, $_);
                }
                    $_ = <$fh>; #advance cursor
            }
        if (!$num) { print "no result found. $_ \n"; }
        }
    }
}

sub printAll {
while(<$fh>){
    if ($_ =~ /^\s*$/) {
        #whitespace
        print $_;
        next;
    }

    if ($_ =~ /^\s*#/) {
        #comments
        next;
    }
    
    if ($_ =~ /^\[(.*)\]\s*$/) {
        #section
        print $_;
        next;
    }

    if ($_ =~ /^([^=]+?)\s*=\s*(.*?)\s*$/) {
        #line
        printf("%s: %s\n", $1, $2);
        next;
    }    
  }
}

sub parseOptions {
    if ($help) { printHelp(); }
    if ($list) { listCharacters(); }
}

sub listCategories {
    print "listCategories\n" if $debug;
    seek($fh,0,0);
    while(<$fh>) {
        print $_ if $_ =~ /^\[(.*)\]\s*$/;
    }
}

sub listCharacters {
    opendir $dh, "$data/$dir";
    @files = readdir $dh;

    foreach (@files) {
        if (-d $_ ) { next };
        if (/^[.]/) { next };
        if (/(.+)[.]$ext$/){ print "$1\n" };
    }

    closedir $dh;
    exit;
}

sub getCharacter {
    opendir $dh, "$data/$dir";
    @files = readdir $dh;
    my @match;
    
    foreach (@files) {
        # if no level, get filename+highest _level
        if ($_ =~ /$filename[_][0-9]+(.*)[.]$ext/) {
            push(@match, $_);
        } elsif (-e "$data/$dir/$filename" . "." . "$ext" ) {
            # if no level and no _level, assume exact match
            $filename = "$data/$dir/$filename" . "." . "$ext";
            push(@match, $filename);
        } 
    }

    my @f = sort @match;
    $filename = $f[-1] or die "No valid character sheet found.\n";
}
